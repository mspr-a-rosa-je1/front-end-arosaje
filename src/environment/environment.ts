export const environment = {
   backendHost : '127.0.0.1',
   backendPort : '8000',
   websocketHost : '127.0.0.1',
   websocketPort : '5000',
   activeStatus : 9
};
