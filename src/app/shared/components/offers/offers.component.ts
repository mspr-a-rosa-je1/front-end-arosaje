import { Component, Input } from '@angular/core';
import { SearchTag } from '@app/core/models/utils';
import { FetchService } from '@core/services/fetch-service/fetch.service';
import { Offer, Tag } from '@features/offer/models/out/offer';
import { firstValueFrom } from 'rxjs';

@Component({
  selector: 'app-offers',
  templateUrl: './offers.component.html',
  styleUrl: './offers.component.scss'
})
export class OffersComponent {
  constructor(private request : FetchService) {}
  @Input() url: string = '';
  @Input() imagePosition: 'left' | 'right' = 'right';
  @Input() searchBar: boolean = false;
  @Input() title: string = '';
  @Input() addButton: boolean = true;

  loading : boolean = true;
  error : boolean = false;
  offers? : Offer[];
  allTags : Tag[] = [];
  
  async ngOnInit() {
    try {
      this.offers = await firstValueFrom(this.request.get<Offer[]>(this.url));
      if(this.searchBar) {
        this.allTags = await firstValueFrom(this.request.get<Tag[]>(`/api/v1/offers/tags`));
      }
    }
    catch(error) {
      this.error = true;
    }
    this.loading = false;
  }

  async filterByTags(tags: SearchTag[]) {
    this.offers = await firstValueFrom(this.request.get<Offer[]>(`${this.url}&tags=${tags.map(t => t.name).join(',')}`));
  }
}
