import { Component, EventEmitter, Input, Output } from '@angular/core';
import { SearchTag } from '@app/core/models/utils';

@Component({
  selector: 'app-search-tags',
  templateUrl: './search-tags.component.html',
  styleUrl: './search-tags.component.scss'
})
export class SearchTagsComponent {
  @Input() allTags : SearchTag[] = [];
  @Input() barPosition : 'start' | 'end' | 'center' = 'center';
  @Input() error? : string;
  @Output() tagsChange = new EventEmitter<SearchTag[]>();
  @Input() tags : SearchTag[] = [];

  results : SearchTag[] = [];
  search : string = '';

  async removeTag(tag: SearchTag) {
    this.tags = this.tags.filter(t => t !== tag);
    this.tagsChange.emit(this.tags);
  }

  async addTag(tag: SearchTag) {
    this.search = '';
    this.results = [];
    if(this.tags.length < 13) {
      this.tags.push(tag);
      this.tagsChange.emit(this.tags);
    }
  }

  async onSearch() {
    if(this.search.length > 0) {
      this.results = this.allTags.filter(tag => tag.name.toLowerCase().includes(this.search.toLowerCase())).slice(0, 5);
      this.results = this.results.filter(result => !this.tags.some(tag => tag.name === result.name));
    }
    else {
      this.results = [];
    }
  }
}
