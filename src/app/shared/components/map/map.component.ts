import { Component, AfterViewInit, Input } from '@angular/core';
import * as L from 'leaflet';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrl: './map.component.scss'
})
export class MapComponent implements AfterViewInit {
  @Input() coordinates: {image : string, lat: number, lng: number }[] = [];
  
  initializeMap(): void {
    const map = L.map('map').setView([48.8584, 2.2945], 6); 

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '© OpenStreetMap contributors'
    }).addTo(map);
  
    const icon = L.icon({
      iconUrl: 'assets/icons/position.svg', 
      iconSize: [32, 32], 
      iconAnchor: [16, 32], 
      popupAnchor: [0, -32] 
    });

    this.coordinates.forEach(coord => {
      L.marker([coord.lat, coord.lng], {icon: icon}).addTo(map)
        .bindPopup(`<img src="data:image/jpeg;base64,${coord.image}" width="100px">`).openPopup();
    });
  }

  async ngAfterViewInit(): Promise<void> {
    setTimeout(() => {
      this.initializeMap();
    }, 1000);
  }
}