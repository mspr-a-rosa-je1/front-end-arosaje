import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrl: './page.component.scss'
})
export class PageComponent {
  @Input() loading: boolean = false;
  @Input() error: boolean = false;
  @Input() top: boolean = true;
  @Input() imageSize: 'small' | 'medium' | 'large' | 'none' = 'none';
  @Input() imagePosition: 'left' | 'right' = 'left';
  @Input() imageSrc?: string;
  @Input() page?: 'fixed' | 'scroll' = 'fixed';
}
