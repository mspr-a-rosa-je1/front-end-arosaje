import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { Advice, Media, Offer } from '@app/features/offer/models/out/offer';


export interface MediaIn {
  id? : number;
  name : string;
  longitude : string;
  latitude : string;
  date? : Date;
  image : string;
  advices? : Advice[];
  offer? : Offer;
}
@Component({
  selector: 'app-pictures',
  templateUrl: './pictures.component.html',
  styleUrl: './pictures.component.scss'
})
export class PicturesComponent {
  @Input() pictures : MediaIn[] = [];
  @Input() editable : boolean = false;
  @Input() title : string = '';
  @Input() errorMessage? : string;
  @Input() validate? : boolean = false;
  @Input() button? : boolean = false;

  @ViewChild('inputFile') inputFile!: ElementRef;
  @ViewChild('camera') camera!: ElementRef;

  @Output() imagesChange = new EventEmitter<MediaIn[]>();
  @Output() validateChange = new EventEmitter<MediaIn[]>();
  @Output() comment = new EventEmitter<number>();

  loading: boolean = false;
  error : boolean = false;
  showButtons : boolean = true;
  videoStream: MediaStream | null = null;

  clickInputFile() {
    this.inputFile.nativeElement.click()
  }

  async validatePictures() {
    this.validateChange.emit(this.pictures);
  }

  async addPictures() {
    const location = await this.getLocation();
    Array.from(this.inputFile.nativeElement.files as FileList).forEach(async (file: File) => {
        const reader = new FileReader();
        reader.onload = () => {
            const base64Data = reader.result as string;
            this.pictures.push({
                name: Math.random().toString(36).slice(2, 72) + '.' + file.name.split('.').pop(),
                image: base64Data,
                latitude: location.latitude,
                longitude: location.longitude
            });
            this.imagesChange.emit(this.pictures);
        };
        reader.readAsDataURL(file);
    });
}

  async startCamera() {
    this.videoStream = await navigator.mediaDevices.getUserMedia({video: true});
    const video = this.camera.nativeElement;
    video.srcObject = this.videoStream;
    await new Promise<void>(resolve => {video.onloadedmetadata = () => {video.play(); resolve();}});
  }

  stopCamera() {
    if(this.videoStream) {
      this.videoStream.getTracks().forEach(track => track.stop());
    }
  }

  async getLocation() : Promise<{latitude: string, longitude: string}> {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(
        pos => resolve({latitude: pos.coords.latitude.toString(), longitude: pos.coords.longitude.toString()}),
        error => reject(error)
      );
    });
  }

  takePicture() {
    const canvas = document.createElement('canvas');
    canvas.width = this.camera.nativeElement.videoWidth;
    canvas.height = this.camera.nativeElement.videoHeight;
    const context = canvas.getContext('2d');

    if(context) {
      context.drawImage(this.camera.nativeElement, 0, 0, canvas.width, canvas.height);
      return canvas.toDataURL('image/jpeg');
    }
    return null;
  }

  processURL(image : string) {
    return 'url(' + (image.match(/^data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,/) ? '' : 'data:image/jpeg;base64,') + image + ')';
  }


  async takePictureWithLocation() {
    try {
      await this.startCamera();
      const location = await this.getLocation()
      const image = this.takePicture();
      if(image) {
        this.pictures.push({name : Math.random().toString(36).slice(2, 72) + '.jpeg', image, latitude: location.latitude, longitude: location.longitude});
        this.imagesChange.emit(this.pictures);
      }
    } 
    catch(error) {
      this.error = true;
    }
  } 

  rmPicture(index : number) {
    this.pictures.splice(index, 1);
    this.imagesChange.emit(this.pictures);
  }

  clickComment(id : number) {
    this.comment.emit(id);
  }
}
