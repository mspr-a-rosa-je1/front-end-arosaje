import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrl: './button.component.scss'
})
export class ButtonComponent {
  @Input() text : string = 'Click me';
  @Input() width : string = '15vw';
  @Input() delete : boolean = false;
}
