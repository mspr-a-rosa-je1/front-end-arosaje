import { ChangeDetectorRef, Component } from '@angular/core';
import { AuthService } from '@app/core/services/auth-service/auth.service';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrl: './navbar.component.scss'
})
export class NavbarComponent {
  constructor(public auth : AuthService) {}

  barsIcon = faBars;

  openMenu() {
    const x = document.getElementById("buttonNavbar");
    if (x?.className === "button-navbar") {
      x!.className += " responsive";
    } else {
      x!.className = "button-navbar";
    }
  }
}
