import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AuthService } from '@app/core/services/auth-service/auth.service';
import { FetchService } from '@app/core/services/fetch-service/fetch.service';
import { SocketService } from '@app/core/services/socket-service/socket.service';
import { ValidatorService } from '@app/core/services/validator-service/validator.service';
import { Message } from '@app/features/user/models/in/user';
import { firstValueFrom } from 'rxjs';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrl: './chat.component.scss'
})
export class ChatComponent {
  @Input() contact? : {id : number, username : string};
  @ViewChild("chat") chat! : ElementRef;

  constructor(private socket : SocketService, private request : FetchService, public auth : AuthService, private validator : ValidatorService) {
    this.validator.setForm(this.messageForm);
  }

  messages? : Message[];
  writing : boolean = false;
  message? : string;
  error?: any;
  messageForm : FormGroup = new FormGroup({
    message : this.validator.createFormControl(this.message, 'text'),
  });
  loading : boolean = true;

  

  async ngOnInit() {
    await this.getConversation();
    this.scrollToBottom();
  }

  async ngOnChanges() {
    this.writing = false;
    this.socket.disconnect();
    await this.getConversation();
  }

  scrollToBottom() {
    const chat = document.getElementById("chat");
    if(chat) {
      chat.scrollTop = chat.scrollHeight;
    } 
  }

  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  async getConversation() {
    this.loading = true;
    try {
      const jwt = localStorage.getItem('access_token');

      if(!this.contact?.id || !this.auth.user?.id || !jwt) {
        throw new Error("test");
      }

      this.messages = (await firstValueFrom(this.request.get<Message[]>(`/api/v1/messages/${this.contact.id}`)))?.slice()?.reverse();
      
      this.socket.connect({
        id : this.auth.user.id.toString(),
        contactId : this.contact.id.toString(),
        jwt
      });

      this.socket.listenEvent("writing").subscribe(
        () => {
          if(this.writing === true) {
            return;
          } 
          else {
            this.writing = true;
            this.onWriting();
          }
        }
      );

      this.socket.listenEvent("notWriting").subscribe(
        () => {this.writing = false; this.messages?.pop()}
      );

      this.socket.listenEvent("message").subscribe(
        (msg) => {this.messages = [...(this.messages ?? []), msg]}
      );

      this.socket.listenEvent("error").subscribe(
        (e) => {this.error = e}
      );

      this.loading = false;
    }
    catch(error) {
      console.error(error)
    }
  }

  onWriting() {
    this.messages?.push({content : ".", senderUsername : this.contact?.username ?? ""});
    const interval = setInterval(() => {
      if (this.writing) { // Condition de la boucle
        this.messages![this.messages!.length - 1].content.length < 3 ? this.messages![this.messages!.length - 1].content += "." : this.messages![this.messages!.length - 1].content = "."; // Modifier le dernier élément
      } else {
        clearInterval(interval); // Arrêter les modifications après 5 itérations
      }
    }, 500); // In
  }

  ngOnDestroy() {
    this.socket.disconnect();
  }

  onInput() {
    this.socket.sendEvent('writing');
  }

  onBlur() {
    if(this.message && this.message.length < 1 || !this.message) {
      this.socket.sendEvent('notWriting');
    }
  }

  onSubmit() {
    if(this.message && this.auth.user?.username) {
      this.socket.sendEvent('message', {
        content : this.message,
        sentAt : new Date(),
        senderUsername : this.auth.user?.username
      });

      this.message = undefined;
      this.socket.sendEvent('notWriting', "");
    }
  }

  isNewDay(i : number) {
    return this.messages && this.messages[i - 1] && new Date(this.messages[i - 1]?.sentAt ?? "").getDay() !== new Date(this.messages[i]?.sentAt ?? "").getDay();
  }
}
