import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './components/footer/footer.component';
import { MapComponent } from './components/map/map.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ErrorComponent } from './components/error/error.component';
import { LayoutComponent } from './components/layout/layout.component';
import { RouterModule } from '@angular/router';
import { PageComponent } from './components/page/page.component';
import { ButtonComponent } from './components/button/button.component';
import { OffersComponent } from './components/offers/offers.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SearchTagsComponent } from './components/search-tags/search-tags.component';
import { PicturesComponent } from './components/pictures/pictures.component';
import { ChatComponent } from './components/chat/chat.component';



@NgModule({
  declarations: [
    FooterComponent,
    MapComponent,
    NavbarComponent,
    ErrorComponent,
    LayoutComponent,
    PageComponent,
    ButtonComponent,
    OffersComponent,
    SearchTagsComponent,
    PicturesComponent,
    ChatComponent,
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    FooterComponent,
    NavbarComponent,
    ErrorComponent,
    LayoutComponent,
    PageComponent,
    ButtonComponent,
    MapComponent,
    OffersComponent,
    SearchTagsComponent,
    PicturesComponent,
    ChatComponent
  ]
})
export class SharedModule {}
