export interface SearchTag {
   name : string;
   id : number;
}

export interface Country {
   name : string;
   states : {name: string}[];
}