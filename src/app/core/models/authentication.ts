export interface Login {
   email : string;
   password : string;
}

export interface Token {
   token : string;
}

export interface Valid {
   isValid : boolean;
}

export interface Register {
   username: string;
   firstName: string;
   lastName: string;
   phone: string;
   email: string;
   password?: string;
   confirmPassword?: string;
}
