import { inject } from "@angular/core";
import { Router } from "@angular/router";
import { AuthService } from "../services/auth-service/auth.service";

export const AuthGuard = async() => {
  const auth = inject(AuthService);
  const router = inject(Router);

  if(!(await auth.isAuthenticated())) {
    router.navigate(['home']);
    return false;
  }
  return true;
}