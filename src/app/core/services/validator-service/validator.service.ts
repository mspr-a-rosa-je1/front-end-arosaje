import { Injectable } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';

type FormControlType = 'email' | 'password' | 'confirmPassword' | 'phone' | 'username' | 'text' | 'check' | 'postalCode' | 'number' | 'futureDate' | 'oneEntryArray' | 'endDate' | 'rating';

let keyToRegex : {[key : string]: RegExp} = {
  password : /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[\W_]).+$/,
  number : /^[0-9]*$/,
  rating : /^[1-5]$/
}

@Injectable({
  providedIn: 'root'
})
export class ValidatorService {
  
  private form?: FormGroup;

  setForm(form: FormGroup) {
    this.form = form;
    if(this.form?.get('password') && this.form?.get('confirmPassword')) {
      this.form.get('password')?.valueChanges.subscribe(value => {
        const control = this.form?.get('confirmPassword');
        if(value) {
          control?.addValidators([Validators.required]);
        } 
        else {
          control?.removeValidators([Validators.required]);
        }
        control?.updateValueAndValidity();
      }
    )
    }
  }

  removeForm() {
    this.form = undefined;
  }
  
  public findFormErrors(): {[key: string]: string} {
    const errors: { [key: string]:  string} = {};
    if(this.form) {
      Object.keys(this.form.controls).forEach(key => {
        const control = this.form?.get(key);
        if(control && control.errors && Object.keys(control.errors).length > 0) {
          errors[key] =  this.errorsToString(control.errors);
        }
      });
    }
    return errors;
  }

  errorsToString(errors : ValidationErrors) : string {
    let errorString = '';
    if(errors['required']) {
      errorString += 'Le champs est requis';
    }
    else {
      errorString += 'Le champs doit contenir ';
      Object.keys(errors).forEach((key, i) => {
        if(key === 'minlength') {
          errorString += 'minimum ' + errors[key]['requiredLength'] + ' caractères';
        }
        if(key === 'maxlength') {
          errorString += 'maximum ' + errors[key]['requiredLength'] + ' caractères';
        }
        if(key === 'password') {
          errorString += 'une majuscule, un chiffre et un caractère spécial';
        }
        if(key === 'email') {
          errorString += "une addresse email valide";
        }
        if(key === 'number') {
          errorString += "uniquement des chiffres";
        }
        if(key === 'confirmPassword') {
          errorString += "le même mot de passe que le champ 'mot de passe'";
        }
        if(key === 'futureDate') {
          errorString += "une date future";
        }
        if(key === 'oneEntryArray') {
          errorString += "au moins une entrée";
        }
        if(key === 'endDate') {
          errorString += "une date postérieure à la date de début";
        }
        if(key === 'rating') {
          errorString += "une note entre 1 et 5";
        }
        if(i < Object.keys(errors).length - 1) {
          errorString += ', ';
        }
      });
    }
    return errorString;
  }

  createFormControl(value : any, type : FormControlType, options? : {required :  boolean}) : FormControl {
    let control : FormControl;
    switch(type) {
      case 'email':
        control =  new FormControl(value, [
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(312),
          Validators.email
        ]);
        break;
      case 'password':
        control = new FormControl(value, [
          ...(options?.required ? [Validators.required] : []),
          /* Validators.minLength(12), */
          Validators.maxLength(64),
          /*this.regexValidator(type)*/
        ]);
        break;
      case 'phone':
        control = new FormControl(value, [
          Validators.required,
          Validators.minLength(10),
          Validators.maxLength(10),
          this.regexValidator('number')
        ]);
        break;
      case 'username':
        control = new FormControl(value, [
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(64)
        ]);
        break;
      case 'text':
        control = new FormControl(value, [
          Validators.required,
          Validators.minLength(2),
        ]);
        break;
      case 'confirmPassword':
        control = new FormControl(value, [
          this.confirmPasswordValidator()
        ]);
        break;
      case 'check':
        control = new FormControl(value, [
          Validators.requiredTrue
        ]);
        break;
      case 'postalCode':
        control = new FormControl(value, [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(5),
          this.regexValidator('number')
        ]);
        break;
      case 'number':
        control = new FormControl(value, [
          Validators.required,
          Validators.minLength(1),
          this.regexValidator('number')
        ]);
        break;
      case 'futureDate':
        control = new FormControl(value, [
          Validators.required,
          this.futureDateValidator()
        ]);
        break;
      case 'endDate':
        control = new FormControl(value, [
          this.futureDateValidator(),
          this.endDateValidator()
        ]);
        break;
      case 'oneEntryArray':
        control = new FormControl(value, [
          this.oneEntryArrayValidator()
        ]);
        break;
      case 'rating':
        control = new FormControl(value, [
          Validators.required,
          this.regexValidator('rating')
        ]);
        break;
      default:
        throw new Error('Invalid formcontrol type');               
    }
    return control;
  }

  regexValidator(type : string): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const isValid = keyToRegex[type].test(control.value);
      return !isValid ? {[type] : {value: control.value}} : null;
    };
  }

  confirmPasswordValidator() : ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const isValid = control.value === this.form?.get('password')?.value;
      return !isValid ? {'confirmPassword' : {value: control.value}} : null;
    };
  }

  futureDateValidator() : ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const isValid = new Date(control.value) > new Date();
      return !isValid && control.value ? {'futureDate' : {value: control.value}} : null;
    };
  }

  endDateValidator() : ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const isValid = new Date(control.value) > new Date(this.form?.get('startDate')?.value);
      return !isValid && control.value ? {'endDate' : {value: control.value}} : null;
    };
  }

  oneEntryArrayValidator() : ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const isValid = Array.isArray(control.value) && control.value.length > 0;
      return !isValid ? {'oneEntryArray' : {value: control.value}} : null;
    };
  }
}
