import { Injectable } from '@angular/core';
import { jwtDecode } from 'jwt-decode';
import { BehaviorSubject, Subject, firstValueFrom } from 'rxjs';
import { Login, Register } from '../../models/authentication';
import { Token, Valid } from '../../models/authentication';
import { Router } from '@angular/router';
import { FetchService } from '../fetch-service/fetch.service';
import { User } from '@app/features/user/models/out/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private request : FetchService, private router : Router) {}
  public user? : User;

  async login(credentials : Login) {
    let res = await firstValueFrom(this.request.post<Login, Token>(`/api/v1/users/login`, credentials));
    if(res.token) {
      localStorage.setItem('access_token', res.token);
      this.user = await firstValueFrom(this.request.get<User>(`/api/v1/users/profile`));
      this.router.navigate(['offers']);
    }
  }

  async register(informations : Register) {
    let res = await firstValueFrom(this.request.post<Register, Token>(`/api/v1/users/register`, informations));
    if(res.token) {
      localStorage.setItem('access_token', res.token);
      this.user = await firstValueFrom(this.request.get<User>(`/api/v1/users/profile`));
      this.router.navigate(['offers']);
    }
  }

  logout() {
    if(localStorage.getItem('access_token')) {
      localStorage.removeItem('access_token');
      this.user = undefined;
      this.router.navigate(['home']);
    }
  }

  async isAuthenticated(): Promise<boolean> {
    let isAuthenticated : boolean;
    
    try {
      let token = localStorage.getItem('access_token');
      
      if(!token) {
        throw Error();
      } 

      isAuthenticated = (await firstValueFrom(this.request.post<{email : string}, Valid>(`/api/v1/users/validate`, {email : this.decryptToken(token).email}))).isValid;
      
      if(isAuthenticated) {
        this.user = await firstValueFrom(this.request.get<User>(`/api/v1/users/profile`));
      }
    }
    catch (error) {
      this.user = undefined;
      isAuthenticated = false;
    }
    return isAuthenticated;
  }

  decryptToken(token : string) : {email : string, exp : number, iat : number, roles : string[]} {
    try {
      return jwtDecode(token);
    } 
    catch(error) {
      throw Error('Cant decrypt token');
    }
  }
}
