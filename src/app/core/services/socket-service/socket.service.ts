import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { environment } from '../../../../environment/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  private socket? : Socket;
  private url : string = 'http://' + environment.websocketHost + ':' + environment.websocketPort;

  constructor() {}

  connect(datas : {[key : string] : string}) {
    this.socket = new Socket({
      url : this.url, 
      options : {
        query : {
          ...datas
        }
      }
    });
    
    this.socket.on('connect_error', (error: any) => {
      console.error('Error during connection :', error);
    });

    this.socket.on('error', (error: any) => {
      console.error('Error from websocket :', error);
    });
  }

  disconnect() {
    this.socket = undefined;
  }

  sendEvent(eventName : string, msg? : string | object) {
    if(!this.socket) {
      throw new Error("No socket connection")
    }
    this.socket.emit(eventName, msg);
  }

  listenEvent(eventName : string): Observable<any> {
    if(!this.socket) {
      throw new Error("No socket connection");
    }
    return this.socket.fromEvent(eventName);
  }
}
