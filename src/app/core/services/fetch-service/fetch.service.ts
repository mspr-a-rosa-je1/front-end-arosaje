import { Injectable } from '@angular/core';
import { Observable, catchError, map, shareReplay, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../../../environment/environment';
import { Country } from '@app/core/models/utils';

@Injectable({
  providedIn: 'root'
})
export class FetchService {
  constructor(private http: HttpClient) {}

  #url = 'http://' + environment.backendHost + ':' + environment.backendPort;
  private countries$?: Observable<Country[]>;

  getCountriesAndStates(): Observable<Country[]> {
    if (!this.countries$) {
      this.countries$ = this.http.get<{data : Country[]}>("https://countriesnow.space/api/v0.1/countries/states")
        .pipe(
          map(res => res['data']),
          catchError(this.handleError),
          shareReplay(1)
      );
    }
    return this.countries$;
  }

  get<T>(path : string) {
    return this.http.get<T>(this.#url + path)
      .pipe(
        catchError(this.handleError)
    );
  }

  post<T, K>(path: string, object : T): Observable<K> {
    return this.http.post<K>(this.#url + path, object)
      .pipe(
        catchError(this.handleError)
    );
  }

  patch<T, K>(path: string, object : T): Observable<K> {
    return this.http.patch<K>(this.#url + path, object)
      .pipe(
        catchError(this.handleError)
    );
  }

  put<T, K>(path: string, object : T): Observable<K> {
    return this.http.put<K>(this.#url + path, object)
      .pipe(
        catchError(this.handleError)
    );
  }

  delete(path: string): Observable<number> {
    return this.http.delete<number>(this.#url + path)
      .pipe(
        catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    return throwError(() => new Error(error.message));
  }
}
