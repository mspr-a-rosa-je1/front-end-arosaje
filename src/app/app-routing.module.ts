import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './core/guards/auth.guard';
import { ErrorComponent } from './shared/components/error/error.component';

const routes: Routes = [
  {
    path: "",
    loadChildren: () => import('./features/unauthenticated/unauthenticated.module').then(m => m.UnauthenticatedModule)
  },
  {
    path: "",
    loadChildren: () => import('./features/user/user.module').then(m => m.UserModule),
    canActivate: [AuthGuard]
  },
  {
    path: "",
    loadChildren: () => import('./features/offer/offer.module').then(m => m.OfferModule),
    canActivate: [AuthGuard]
  },
  {
    path: "**",
    component: ErrorComponent,
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    bindToComponentInputs: true,
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
