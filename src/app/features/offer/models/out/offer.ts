
export interface Offer {
   id : number;
   title : string;
   publicationDate : Date;
   keeperRating : number;
}

export interface AddressDetail {
   city : string;
   postalCode : string;
   country : string;
}

export interface Address {
   id : number;
   city : string;
   postalCode : string;
   country : string;
}

export interface Media {
   id : number;
   name : string;
   longitude : string;
   latitude : string;
   date : Date;
   image : string;
}

export interface User {
   id : number;
   username : string;
   firstName : string;
   lastName : string;
}

export interface OfferDetail {
   id : number;
   title : string;
   description : string;
   publicationDate : string;
   startDate : string;
   endDate : string;
   keeperRating : number;
   address : Address;
   medias : MediaDetail[];
   plants : Plant[];
   tags : Tag[];
   status : Status;
   publisher : User;
   keeper : User;
}

export interface Status {
   name : string;
   id : number;
}

export interface Plant {
   name : string;
   id : number;
}

export interface Tag {
   name : string;
   id : number;
}

export interface MediaDetail {
   id : number;
   name : string;
   longitude : string;
   latitude : string;
   date : Date;
   image : string;
   advices : Advice[];
   offer : Offer;
}

export interface Advice {
   id : number;
   content : string;
}

export interface UserDetail {
   id : number;
   username : string;
   firstName : string;
   lastName : string;
   phone : string;
   email : string;
   advices : string[];
   offersPublisher : Offer[];
   offersKeeper : Offer[];
   role : Role;
}

export interface Role {
   id: number;
   name : string;
}

export interface Offer {
   id : number;
   title : string;
   publicationDate : Date;
   keeperRating : number;
}