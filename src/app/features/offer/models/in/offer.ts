import { Plant, Tag } from "../out/offer";

export interface Offer {
   id : number;
   title : string;
   publicationDate : Date;
   keeperRating : number;
}

export interface AddressDetail {
   city : string;
   postalCode : string;
   country : string;
}

export interface Address {
   id : number;
   city : string;
   postalCode : string;
   country : string;
}

export interface Media {
   name : string;
   longitude : string;
   latitude : string;
   image : string;
}

export interface User {
   id : number;
   username : string;
   firstName : string;
   lastName : string;
}

export interface AddressDetail {
   city : string;
   postalCode : string;
   country : string;
}

export interface OfferDetailPost {
   title : string;
   id? : number;
   description : string;
   startDate : string;
   endDate? : string ;
   address : AddressDetail;
   medias : Media[];
   plants : Plant[];
   tags : Tag[];
   status : {id : number};
}