import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AllOffersComponent } from './components/offers/offers.component';
import { OffersDetailsComponent } from './components/offers-details/offers-details.component';
import { MyoffersComponent } from './components/myoffers/myoffers.component';
import { OffersAddComponent } from './components/offers-add/offers-add.component';
import { OffersEditComponent } from './components/offers-edit/offers-edit.component';
import { MyoffersDetailsComponent } from './components/myoffers-details/myoffers-details.component';
import { MykeepingComponent } from './components/mykeeping/mykeeping.component';
import { DetailsComponent } from './components/mykeeping/details/details.component';
import { AdvicesComponent } from './components/advices/advices.component';
import { RequestComponent } from './components/request/request.component';
import { RatingComponent } from './components/rating/rating.component';
import { AddPictureComponent } from './components/add-picture/add-picture.component';

const routes : Routes = [
  {
    path: "offers",
    component: AllOffersComponent,
  },
  {
    path: "offers/details/:id",
    component: OffersDetailsComponent,
  },
  {
    path: "myoffers",
    component: MyoffersComponent,
  },
  {
    path: "offers/add",
    component: OffersAddComponent,
  },
  {
    path: "offers/edit/:id",
    component: OffersEditComponent,
  },
  {
    path: "myoffers/details",
    component: MyoffersDetailsComponent,
  },
  {
    path: "mykeeping",
    component: MykeepingComponent,
  },
  {
    path: "mykeeping/details",
    component: DetailsComponent,
  },
  {
    path: "offers/advice/:id",
    component: AdvicesComponent,
  },
  {
    path: "myoffer/id/request",
    component: RequestComponent,
  },
  {
    path: "offers/rating/:id",
    component: RatingComponent,
  },
  {
    path: "mykeeping/id/addpicture",
    component: AddPictureComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OfferRoutingModule { }
