import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { firstValueFrom } from 'rxjs';
import { MediaDetail } from '../../models/out/offer';
import { ValidatorService } from '@app/core/services/validator-service/validator.service';
import { FetchService } from '@app/core/services/fetch-service/fetch.service';
import { AuthService } from '@app/core/services/auth-service/auth.service';

@Component({
  selector: 'app-advices',
  templateUrl: './advices.component.html',
  styleUrl: './advices.component.scss'
})
export class AdvicesComponent {
  constructor(private auth : AuthService,  private route : ActivatedRoute, private validator : ValidatorService, private router : Router, private request : FetchService) {
    this.validator.setForm(this.adviseForm);
  }

  error : boolean = false;
  loading : boolean = true;
  media? : MediaDetail;
  advise? : string;
  errors : {[key: string]: string} = {};

  adviseForm : FormGroup = new FormGroup({
    advise : this.validator.createFormControl(this.advise, 'text'),
  });

  async ngOnInit() {
    try{
      this.route.params.subscribe(async(params) => {
        this.media = await firstValueFrom(this.request.get<MediaDetail>(`/api/v1/offers/medias/${params['id']}`));
      });
    }
    catch(error) {
      this.error = true;
    }
    
    this.loading = false;
  }

  async onSubmit() {
    if(this.adviseForm.valid) {
      this.loading = true;
      try {
        if(this.advise) {
          await firstValueFrom(this.request.post<{content : string}, {id : number}>(`/api/v1/botanist/offers/add_advice/${this.media?.id}`, {content : this.advise}));
          this.router.navigate(['offers/details/' + this.media?.offer.id]);
        }
      }
      catch(error) {
        this.errors['offer'] = "Impossible d'ajouter le commentaire'";
      }
      this.loading = false;
    }
  }

  onInput() {
    this.errors = this.validator.findFormErrors();
  }

  processURL(image : string) {
    return 'url(' + (image.match(/^data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,/) ? '' : 'data:image/jpeg;base64,') + image + ')';
  }
}
