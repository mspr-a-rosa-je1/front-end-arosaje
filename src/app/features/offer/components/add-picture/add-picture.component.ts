import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-picture',
  templateUrl: './add-picture.component.html',
  styleUrl: './add-picture.component.scss'
})
export class AddPictureComponent implements OnInit{

  @ViewChild('inputFile') inputFile!: ElementRef;
  @ViewChild('capturedPhoto') capturedPhoto!: ElementRef;
  @ViewChild('videoElement') videoElement!: ElementRef;
  videoStream: MediaStream | null = null;
  constructor() { }
  clickInputFile() {
    this.inputFile.nativeElement.click()
  }
  ngOnInit(): void {
    this.startCamera(); 
  }
  startCamera() {
    navigator.mediaDevices.getUserMedia({ video: true })
      .then((stream) => {
        this.videoStream = stream;
        const video = this.videoElement.nativeElement;
        video.srcObject = stream;
        video.play();
      })
      .catch((error) => {
        console.error('Error accessing media devices:', error);
      });
  }
  stopCamera() {
    if (this.videoStream) {
      this.videoStream.getTracks().forEach(track => track.stop());
    }
  }
  takePictureWithLocation() {
    navigator.geolocation.getCurrentPosition((position) => {
      const coords = {
        latitude: position.coords.latitude,
        longitude: position.coords.longitude
      };

      // Now you have the coordinates, you can use them to do whatever you need.
      console.log('Latitude:', coords.latitude);
      console.log('Longitude:', coords.longitude);

      // Proceed with taking the picture using the obtained location...
      const capturedPhoto = this.capturedPhoto.nativeElement;
      const options = {
        video: true,
        audio: false
      };

      navigator.mediaDevices.getUserMedia(options)
        .then((stream) => {
          const video = document.createElement('video');
          document.body.appendChild(video);
          video.srcObject = stream;
          video.play();

          video.onloadedmetadata = () => {
            const canvas = document.createElement('canvas');
            canvas.width = video.videoWidth;
            canvas.height = video.videoHeight;
            const context = canvas.getContext('2d');

            if (context) {
              context.drawImage(video, 0, 0, canvas.width, canvas.height);

              const dataUrl = canvas.toDataURL('image/jpeg');
              capturedPhoto.src = dataUrl;
              capturedPhoto.style.display = 'block';
              stream.getTracks().forEach(track => track.stop());
              document.body.removeChild(video);
            } else {
              console.error('Canvas context not found.');
            }
          };
        })
        .catch((error) => {
          console.error('Error accessing media devices:', error);
        });

    }, (error) => {
      console.error('Error getting geolocation:', error);
    });
  }

  ngAfterViewInit(): void {
    const takePictureBtn = document.getElementById('take-picture-btn');
    const capturedPhoto = this.capturedPhoto.nativeElement;

    if (takePictureBtn) {
      takePictureBtn.addEventListener('click', () => {
        this.takePictureWithLocation();
      });
    } else {
      console.error('The "take-picture-btn" button was not found in the DOM.');
    }
  }
}