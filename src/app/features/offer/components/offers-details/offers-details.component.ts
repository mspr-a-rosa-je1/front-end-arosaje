import { Component } from '@angular/core';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { MediaDetail, OfferDetail, Status, UserDetail } from '../../models/out/offer';
import { FetchService } from '../../../../core/services/fetch-service/fetch.service';
import { firstValueFrom } from 'rxjs';
import { environment } from '@env/environment';
import { Media } from '../../models/in/offer';
import { AuthService } from '@app/core/services/auth-service/auth.service';

@Component({
  selector: 'app-offers-details',
  templateUrl: './offers-details.component.html',
  styleUrl: './offers-details.component.scss'
})
export class OffersDetailsComponent {
  constructor(private route : ActivatedRoute, private request : FetchService, public router : Router, public auth : AuthService) {}

  user? : UserDetail;
  offer? : OfferDetail;
  publisherMedias : MediaDetail[] = [];
  keeperMedias : MediaDetail[] = [];
  publisher? : UserDetail;
  coordinates? : {image : string, lat : number, lng : number}[];
  loading : boolean = true;
  error : boolean = false;
  statuses? : Status[];

  async ngOnInit() {
    try {
      this.route.params.subscribe(async(params) => {
        this.user = await firstValueFrom(this.request.get<UserDetail>(`/api/v1/users/profile`));
        
        this.offer = await firstValueFrom(this.request.get<OfferDetail>(`/api/v1/offers/details/${params['id']}`));
        if(this.offer?.medias) {
          this.coordinates = this.offer.medias.map(m => ({image : m.image ?? '', lat: parseFloat(m.latitude), lng: parseFloat(m.longitude)}));
          this.publisherMedias = this.offer.medias.filter(m => new Date(m.date) < new Date(this.offer?.startDate ?? ''));
          this.keeperMedias = this.offer.medias.filter(m => new Date(m.date) >= new Date(this.offer?.startDate ?? ''));
        }
        if(this.offer) {
          this.publisher = await firstValueFrom(this.request.get<UserDetail>(`/api/v1/users/${this.offer?.publisher.id}`));
        }

        this.statuses = await firstValueFrom(this.request.get<Status[]>(`/api/v1/offers/statuses`));
        this.loading = false;
      });
    }
    catch(error) {
      this.error = true;
    }
  }

  async deleteOffer() {
    if(this.offer) {
      this.loading = true;
      try {
        await firstValueFrom(this.request.delete(`/api/v1/offers/${this.offer.id}`));
        this.router.navigate(['/myoffers']);
      }
      catch(error) {
        this.error = true;
      }
      this.loading = false;
    }
  }

  async sendMessage() {
    if(this.offer?.id) 
      this.router.navigate(['/newmessage/' + this.offer.id]);
  }

  async keepOffer() {
    if(this.offer) {
      this.loading = true;
      try {
        await firstValueFrom(this.request.put<{statusId : number}, {id : number}>(`/api/v1/offers/keeper_offer/${this.offer.id}`, {statusId : (this.statuses?.find(s => s.name = "En cours"))?.id ?? -1}));
        this.router.navigate(['/offers/details/' + this.offer.id]);
      }
      catch(error) {
        this.error = true;
      }
      this.loading = false;
    }
  }

  async addPictures(pictures : Media[]) {
    if(this.offer) {
      this.loading = true;
      try {
        await firstValueFrom(this.request.put<{medias : Media[]}, {id : number}>(`/api/v1/offers/keeper_offer_media/${this.offer.id}`, {medias : pictures}));
        window.location.reload();
      }
      catch(error) {
        this.error = true;
      }
      this.loading = false;
      }
    }
}
