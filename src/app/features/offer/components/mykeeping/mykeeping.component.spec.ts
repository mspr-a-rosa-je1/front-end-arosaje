import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MykeepingComponent } from './mykeeping.component';

describe('MykeepingComponent', () => {
  let component: MykeepingComponent;
  let fixture: ComponentFixture<MykeepingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MykeepingComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(MykeepingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
