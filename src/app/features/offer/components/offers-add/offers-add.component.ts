import { Component,ElementRef,ViewChild } from '@angular/core';
import { OfferDetail, Plant, Status, Tag } from '../../models/out/offer';
import { firstValueFrom } from 'rxjs';
import { FetchService } from '@app/core/services/fetch-service/fetch.service';
import { Country, SearchTag } from '@app/core/models/utils';
import { ValidatorService } from '@app/core/services/validator-service/validator.service';
import { FormGroup } from '@angular/forms';
import { OfferDetailPost } from '../../models/in/offer';
import { MediaIn } from '@app/shared/components/pictures/pictures.component';
import {environment} from '@env/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-offers-add',
  templateUrl: './offers-add.component.html',
  styleUrl: './offers-add.component.scss'
})
export class OffersAddComponent {

  constructor(public request: FetchService, private validator : ValidatorService, private router : Router) {
    this.validator.setForm(this.offerForm);
  } 

  error : boolean = false;
  loading : boolean = false;
  allTags : Tag[] = [];
  allPlants : Plant[] = [];
  states : {name : string}[] = [];
  errors : {[key: string]: string} = {};
  countries : Country[] = [];
  statuses? : Status[];

  offer : OfferDetailPost = {
    title : "",
    description : "",
    plants : [],
    startDate : "",
    endDate : undefined,
    tags : [],
    address : {
      country : "",
      city : "",
      postalCode : "",
    },
    medias : [],
    status : {id : environment.activeStatus}
  };

  offerForm: FormGroup = new FormGroup({
    title: this.validator.createFormControl(this.offer.title, 'text'),
    description: this.validator.createFormControl(this.offer.description, 'text'),
    plants: this.validator.createFormControl(this.offer.plants, 'oneEntryArray'),
    startDate: this.validator.createFormControl(this.offer.startDate, 'futureDate'),
    endDate: this.validator.createFormControl(this.offer.endDate, 'endDate'),
    tags: this.validator.createFormControl(this.offer.tags, 'oneEntryArray'),
    country: this.validator.createFormControl(this.offer.address.country, 'text'),
    city: this.validator.createFormControl(this.offer.address.city, 'text'),
    postalCode: this.validator.createFormControl(this.offer.address.postalCode, 'postalCode'),
    medias: this.validator.createFormControl(this.offer.medias, 'oneEntryArray'),
  });

  async ngOnInit() {
    try {
      this.request.getCountriesAndStates().subscribe(countries => this.countries = countries);
      this.allTags = await firstValueFrom(this.request.get<Tag[]>(`/api/v1/offers/tags`));
      this.allPlants = await firstValueFrom(this.request.get<Plant[]>(`/api/v1/offers/plants`));
      this.statuses = await firstValueFrom(this.request.get<Status[]>(`/api/v1/offers/statuses`));
    }
    catch(error) {
      this.error = true;
    }
    this.loading = false;
  }

  async onSubmit() {
    if(this.offerForm.valid) {
      this.loading = true;
      try {
        
        this.offer.status = {id : (this.statuses?.find(s => s.name === "Actif"))?.id ?? -1}
        await firstValueFrom(this.request.post<OfferDetailPost, {id : number}>('/api/v1/offers', this.offer));
        this.router.navigate(['myoffers']);
      }
      catch(error) {
        this.errors['offer'] = "Impossible de créer l'offre.";
      }
      this.loading = false;
    }
  }

  onInput() {
    this.errors = this.validator.findFormErrors();
  }

  onSelectCountry(event: Event) {
    const selectedCountry = this.countries.find(c => c.name === (event.target as HTMLSelectElement).value);
    this.offer.address.country = selectedCountry?.name ?? "";
    this.offerForm.controls['country'].setValue(this.offer.address.country);
    this.states = selectedCountry ? selectedCountry.states : [];
    this.offerForm.markAsDirty();
    this.errors = this.validator.findFormErrors();
  }

  onSelectState(event: Event) {
    this.offerForm.markAsDirty();
    this.errors = this.validator.findFormErrors();
  }

  modifyArray(event: SearchTag[], key : 'plants' | 'tags') {
    this.offer[key] = event;
    this.offerForm.controls[key].setValue(this.offer[key]);
    this.offerForm.markAsDirty();
    this.errors = this.validator.findFormErrors();
  }

  onPicturesChange(event: MediaIn[]) {
    this.offer.medias = event;
    this.offerForm.controls['medias'].setValue(this.offer.medias);
    this.offerForm.markAsDirty();
    this.errors = this.validator.findFormErrors();
  }
}