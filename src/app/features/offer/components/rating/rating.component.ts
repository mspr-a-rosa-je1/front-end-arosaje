import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '@app/core/services/auth-service/auth.service';
import { Offer } from '../../models/out/offer';
import { ValidatorService } from '@app/core/services/validator-service/validator.service';
import { FormGroup } from '@angular/forms';
import { firstValueFrom } from 'rxjs';
import { FetchService } from '@app/core/services/fetch-service/fetch.service';
import { environment } from '@env/environment';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrl: './rating.component.scss'
})
export class RatingComponent {
  constructor(private auth : AuthService,  private route : ActivatedRoute, private validator : ValidatorService, private router : Router, private request : FetchService) {
    this.validator.setForm(this.ratingForm);
  }

  error : boolean = false;
  loading : boolean = true;
  offer? : Offer;
  rating? : number;;
  errors : {[key: string]: string} = {};

  ratingForm : FormGroup = new FormGroup({
    rating : this.validator.createFormControl(this.rating, 'rating'),
  });

  async ngOnInit() {
    this.route.params.subscribe(async(params) => {
      this.offer = this.auth.user?.offersPublisher.find(o => o.id === parseInt(params['id']));
    });
    this.loading = false;

    if(!this.offer) {
      this.error = true;
    }
  }

  async onSubmit() {
    if(this.ratingForm.valid) {
      this.loading = true;
      try {
        await firstValueFrom(this.request.put<{rating : number, statusId : number}, {id : number}>(`/api/v1/offers/keeper_offer_note/${this.offer?.id}`, {rating : this.rating!, statusId : environment.activeStatus}));
        this.router.navigate(['offers/details/' + this.offer?.id]);
      }
      catch(error) {
        this.errors['offer'] = "Impossible de noter le gardien";
      }
      this.loading = false;
    }
  }

  onInput() {
    this.errors = this.validator.findFormErrors();
  }
}
