import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyoffersDetailsComponent } from './myoffers-details.component';

describe('MyoffersDetailsComponent', () => {
  let component: MyoffersDetailsComponent;
  let fixture: ComponentFixture<MyoffersDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MyoffersDetailsComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(MyoffersDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
