import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OffersEditComponent } from './offers-edit.component';

describe('MyoffersEditComponent', () => {
  let component: OffersEditComponent;
  let fixture: ComponentFixture<OffersEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [OffersEditComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(OffersEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
