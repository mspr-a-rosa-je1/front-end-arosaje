import { Component,ElementRef,ViewChild } from '@angular/core';
import { OfferDetail, Plant, Tag } from '../../models/out/offer';
import { firstValueFrom } from 'rxjs';
import { FetchService } from '@app/core/services/fetch-service/fetch.service';
import { Country, SearchTag } from '@app/core/models/utils';
import { ValidatorService } from '@app/core/services/validator-service/validator.service';
import { FormGroup } from '@angular/forms';
import { OfferDetailPost } from '../../models/in/offer';
import { MediaIn } from '@app/shared/components/pictures/pictures.component';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '@env/environment';

@Component({
  selector: 'app-offers-edit',
  templateUrl: './offers-edit.component.html',
  styleUrl: './offers-edit.component.scss'
})
export class OffersEditComponent {

  constructor(public request: FetchService, private validator : ValidatorService, private route : ActivatedRoute, private router : Router) {
    this.validator.setForm(this.offerForm);
  } 

  error : boolean = false;
  loading : boolean = true;
  allTags : Tag[] = [];
  allPlants : Plant[] = [];
  states : {name : string}[] = [];
  errors : {[key: string]: string} = {};
  countries : Country[] = [];

  offer : OfferDetailPost = {
    title : "",
    description : "",
    plants : [],
    startDate : "",
    endDate : undefined,
    tags : [],
    address : {
      country : "",
      city : "",
      postalCode : "",
    },
    medias : [],
    status : {
      id : environment.activeStatus
    }
  };

  offerForm: FormGroup = new FormGroup({
    title: this.validator.createFormControl(this.offer.title, 'text'),
    description: this.validator.createFormControl(this.offer.description, 'text'),
    plants: this.validator.createFormControl(this.offer.plants, 'oneEntryArray'),
    startDate: this.validator.createFormControl(this.offer.startDate, 'futureDate'),
    endDate: this.validator.createFormControl(this.offer.endDate, 'endDate'),
    tags: this.validator.createFormControl(this.offer.tags, 'oneEntryArray'),
    country: this.validator.createFormControl(this.offer.address.country, 'text'),
    city: this.validator.createFormControl(this.offer.address.city, 'text'),
    postalCode: this.validator.createFormControl(this.offer.address.postalCode, 'postalCode'),
    medias: this.validator.createFormControl(this.offer.medias, 'oneEntryArray'),
  });

  async ngOnInit() {
    try {
      this.request.getCountriesAndStates().subscribe(c => this.countries = c);
      this.allTags = await firstValueFrom(this.request.get<Tag[]>(`/api/v1/offers/tags`));
      this.allPlants = await firstValueFrom(this.request.get<Plant[]>(`/api/v1/offers/plants`));

      this.route.params.subscribe(async(params) => {
        const originalOffer = await firstValueFrom(this.request.get<OfferDetail>(`/api/v1/offers/details/${params['id']}`));
        
        this.offer = {
          ...originalOffer, 
          startDate : new Date(originalOffer.startDate).toISOString().split('T')[0],
          endDate : originalOffer.endDate ?  new Date(originalOffer.endDate).toISOString().split('T')[0] : "",
          address : {
            ...originalOffer.address
          }
        } as OfferDetailPost;

        this.onSelectCountry(this.offer.address.country);
        this.offerForm.patchValue(this.offer);
      });
    }
    catch(error) {
      this.error = true;
    }
    this.loading = false;
  }

  async onSubmit() {
    if(this.offerForm.valid) {
      this.loading = true;
      try {
        await firstValueFrom(this.request.put<OfferDetailPost, {id : number}>(`/api/v1/offers/${this.offer.id}`, this.offer));
        window.scrollTo(0, 0);
        this.router.navigate(['offers/edit/' + this.offer.id]);
      }
      catch(error) {
        this.errors['offer'] = "Impossible de créer l'offre.";
      }
      this.loading = false;
    }
  }

  onInput() {
    this.errors = this.validator.findFormErrors();
  }

  onSelectCountry(event: Event | string) {
    const selectedCountry = this.countries.find(c => c.name === (typeof event === 'string' ? event : (event.target as HTMLSelectElement).value));
    this.offer.address.country = selectedCountry?.name ?? "";
    this.offerForm.controls['country'].setValue(this.offer.address.country);
    this.states = selectedCountry ? selectedCountry.states : [];
    if(typeof event !== 'string') {
      this.offerForm.markAsDirty();
    }
    this.errors = this.validator.findFormErrors();
  }

  modifyArray(event: SearchTag[], key : 'plants' | 'tags') {
    this.offer[key] = event;
    this.offerForm.controls[key].setValue(this.offer[key]);
    this.offerForm.markAsDirty();
    this.errors = this.validator.findFormErrors();
  }

  onPicturesChange(event: MediaIn[]) {
    this.offer.medias = event;
    this.offerForm.controls['medias'].setValue(this.offer.medias);
    this.offerForm.markAsDirty();
    this.errors = this.validator.findFormErrors();
  }
}