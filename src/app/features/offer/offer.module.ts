import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddPictureComponent } from './components/add-picture/add-picture.component';
import { AdvicesComponent } from './components/advices/advices.component';
import { MykeepingComponent } from './components/mykeeping/mykeeping.component';
import { MyoffersComponent } from './components/myoffers/myoffers.component';
import {  OffersAddComponent } from './components/offers-add/offers-add.component';
import { MyoffersDetailsComponent } from './components/myoffers-details/myoffers-details.component';
import {  OffersEditComponent } from './components/offers-edit/offers-edit.component';
import { AllOffersComponent } from './components/offers/offers.component';
import { OffersDetailsComponent } from './components/offers-details/offers-details.component';
import { RatingComponent } from './components/rating/rating.component';
import { RequestComponent } from './components/request/request.component';
import { DetailsComponent } from './components/mykeeping/details/details.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SharedModule } from '../../shared/shared.module';
import { OfferRoutingModule } from './offer-routing.module';



@NgModule({
  declarations: [
    AddPictureComponent,
    AdvicesComponent,
    MykeepingComponent,
    MyoffersComponent,
    OffersAddComponent,
    MyoffersDetailsComponent,
    OffersEditComponent,
    AllOffersComponent,
    OffersDetailsComponent,
    RatingComponent,
    RequestComponent,
    DetailsComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    OfferRoutingModule,
    SharedModule
  ]
})
export class OfferModule { }
