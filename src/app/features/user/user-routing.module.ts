import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProfileComponent } from './components/profile/profile.component';
import { EditProfileComponent } from './components/edit-profile/edit-profile.component';
import { OtherProfileComponent } from './components/other-profile/other-profile.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { NewMessageComponent } from './components/new-message/new-message.component';
import { MessagingComponent } from './components/messaging/messaging.component';

const routes : Routes = [
  {
    path: "profile",
    component: ProfileComponent,
  },
  {
    path: "profile/edit",
    component: EditProfileComponent,
  },
  {
    path: "profile/:id",
    component: OtherProfileComponent,
  },
  {
    path: "welcome",
    component: WelcomeComponent,
  },
  {
    path: "newmessage/:id",
    component: NewMessageComponent
  },
  {
    path: "messaging",
    component: MessagingComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
