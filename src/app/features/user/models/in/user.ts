export interface Conversation {
   id : number;
   contactId : number;
   contactUsername : string;
   resumeContent : string;
   sentAt : Date;
}

export interface Message {
   id? : number;
   content : string;
   sentAt? : Date;
   senderUsername : string;
   receiverUsername? : string;
}