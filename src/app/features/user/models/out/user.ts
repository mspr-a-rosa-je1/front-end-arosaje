
export interface User {
   id : number;
   username : string;
   firstName : string;
   lastName : string;
   phone : string;
   email : string;
   advices : string[];
   offersPublisher : Offer[];
   offersKeeper : Offer[];
   roles : string[];
}

export interface Role {
   id: number;
   name : string;
}

export interface Offer {
   id : number;
   title : string;
   publicationDate : Date;
   keeperRating : number;
}