import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditProfileComponent } from './components/edit-profile/edit-profile.component';
import { OtherProfileComponent } from './components/other-profile/other-profile.component';
import { ProfileComponent } from './components/profile/profile.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SharedModule } from '../../shared/shared.module';
import { UserRoutingModule } from './user-routing.module';
import { NewMessageComponent } from './components/new-message/new-message.component';
import { MessagingComponent } from './components/messaging/messaging.component';



@NgModule({
  declarations: [
    EditProfileComponent,
    OtherProfileComponent,
    ProfileComponent,
    WelcomeComponent,
    NewMessageComponent,
    MessagingComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    UserRoutingModule,
    SharedModule
  ]
})
export class UserModule { }
