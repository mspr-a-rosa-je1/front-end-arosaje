import { Component } from '@angular/core';
import { FetchService } from '@app/core/services/fetch-service/fetch.service';
import { User } from '../../models/out/user';
import { firstValueFrom } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { OfferDetail, Plant } from '@app/features/offer/models/out/offer';

@Component({
  selector: 'app-other-profile',
  templateUrl: './other-profile.component.html',
  styleUrl: './other-profile.component.scss'
})
export class OtherProfileComponent {

  constructor(private request : FetchService, private route : ActivatedRoute) {}

  error: boolean = false;
  loading: boolean = true;
  user? : User;
  actualPlants? : {name : string, start : Date, end? : Date, idOffer : number}[] = [];
  keepedPlants? : {name : string, start : Date, end : Date, idOffer : number}[] = [];


  async ngOnInit() {
    try {
      this.route.params.subscribe(async(params) => {
        this.user = await firstValueFrom(this.request.get<User>(`/api/v1/users/${params['id']}`));
        if(this.user) {
          this.user.offersKeeper.forEach(async(o) => {

            let offer = (await firstValueFrom(this.request.get<OfferDetail>(`/api/v1/offers/details/${o.id}`)));

            if(offer && new Date(offer.endDate) < new Date() && offer.keeper) {
              this.keepedPlants?.push(...offer.plants.map(p => ({name : p.name, start : new Date(offer.startDate), end : new Date(offer.endDate), idOffer : o.id})));
            }
            else if(offer && offer.keeper) {
              this.actualPlants?.push(...offer.plants.map(p => ({name : p.name, start : new Date(offer.startDate), end : new Date(offer.endDate), idOffer : o.id})));
            }
          });
        }
      });
    }
    catch(error) {
      this.error = true;
    }
    this.loading = false;
  }
}
