import { Component } from '@angular/core';
import { Conversation } from '../../models/in/user';
import { firstValueFrom } from 'rxjs';
import { FetchService } from '@app/core/services/fetch-service/fetch.service';

@Component({
  selector: 'app-messaging',
  templateUrl: './messaging.component.html',
  styleUrl: './messaging.component.scss'
})
export class MessagingComponent {
  conversations? : Conversation[];
  error : boolean = false;
  loading : boolean = true;
  contact? : {id : number, username : string};

  constructor(private request : FetchService) {}

  async ngOnInit() {
    try {
        this.conversations = await firstValueFrom(this.request.get<Conversation[]>(`/api/v1/messages`));
        
        if(this.conversations.length > 0) {
          this.contact = {id : this.conversations[0].contactId, username : this.conversations[0].contactUsername};
        }
    }
    catch(error) {
      this.error = true;
    }
    this.loading = false;
  }
}
