import { Component } from '@angular/core';
import { FetchService } from '@app/core/services/fetch-service/fetch.service';
import { Conversation } from '../../models/in/user';
import { firstValueFrom } from 'rxjs';
import { ValidatorService } from '@app/core/services/validator-service/validator.service';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { OfferDetail } from '@app/features/offer/models/out/offer';

@Component({
  selector: 'app-new-message',
  templateUrl: './new-message.component.html',
  styleUrl: './new-message.component.scss'
})
export class NewMessageComponent {
  constructor(private request : FetchService, private validator : ValidatorService, private route : ActivatedRoute, private router : Router) {
    this.validator.setForm(this.messageForm);
  }

  
  conversations? : Conversation[];
  offer? : OfferDetail
  error : boolean = false;
  loading: boolean = true;
  message? : string;
  errors : {[key: string]: string} = {};

  messageForm : FormGroup = new FormGroup({
    advise : this.validator.createFormControl(this.message, 'text'),
  });

  async ngOnInit() {
    try {
      this.route.params.subscribe(async(params) => {
        this.offer = await firstValueFrom(this.request.get<OfferDetail>(`/api/v1/offers/details/${params['id']}`));
        this.conversations = await firstValueFrom(this.request.get<Conversation[]>(`/api/v1/messages`));
      })
      
    }
    catch(error) {
      this.error = true;
    }
    this.loading = false;
  }

  async onSubmit() {
    if(this?.message && this.messageForm.valid && this?.offer?.publisher?.id) {
      this.loading = true;
      try {
        await firstValueFrom(this.request.post<{content : string}, {id : number}>(`/api/v1/messages/${this.offer.publisher.id}`, {content : this.message}));
        this.router.navigate(['/messaging']);
      }
      catch(error) {
        this.errors['edit'] = 'Modification impossible';
      }
      this.loading = false;
    }
  }

  onInput() {
    this.errors = this.validator.findFormErrors();
  }
}
