import { Component, OnInit } from '@angular/core';
import { FetchService } from '../../../../core/services/fetch-service/fetch.service';
import { User } from '../../models/out/user';
import { firstValueFrom } from 'rxjs';
import { ValidatorService } from '@app/core/services/validator-service/validator.service';
import { FormGroup } from '@angular/forms';
import { Register } from '@app/core/models/authentication';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrl: './edit-profile.component.scss'
})
export class EditProfileComponent {

  constructor(private request : FetchService, private validator : ValidatorService) {
    this.validator.setForm(this.userForm);
  }

  error: boolean = false;
  loading: boolean = true;
  errors : {[key: string]: string} = {};
  user? : User;

  editedUser : Register = {
    email : "",
    username : "",
    firstName : "",
    lastName : "",
    phone : "",
    password : "",
    confirmPassword : ""
  };

  userForm: FormGroup = new FormGroup({
    email: this.validator.createFormControl(this.editedUser.email, 'email'),
    password: this.validator.createFormControl(this.editedUser.password, 'password', {required : false}),
    confirmPassword: this.validator.createFormControl(this.editedUser.confirmPassword, 'confirmPassword'),
    username: this.validator.createFormControl(this.editedUser.username, 'username'),
    firstName: this.validator.createFormControl(this.editedUser.firstName, 'text'),
    lastName: this.validator.createFormControl(this.editedUser.lastName, 'text'),
    phone: this.validator.createFormControl(this.editedUser.phone, 'phone'),
  });
  
  async ngOnInit() {
    try {
      this.user = await firstValueFrom(this.request.get<User>(`/api/v1/users/profile`));
    
      if(this.user) {
        this.editedUser = {
          email : this.user.email,
          username : this.user.username,
          firstName : this.user.firstName,
          lastName : this.user.lastName,
          phone : this.user.phone,
          password : "",
          confirmPassword : ""
        }
      }
    }
    catch(error) {
      this.error = true;
    }
    this.loading = false;
  }

  async onSubmit() {
    if(this.userForm.valid) {
      this.loading = true;
      try {
        const user = {...this.editedUser};
        delete user.confirmPassword;
        !user.password && (delete user.password);
        await firstValueFrom(this.request.put(`/api/v1/users/profile`, user));
        this.editedUser.password = "";
      }
      catch(error) {
        this.errors['edit'] = 'Modification impossible';
      }
      this.loading = false;
    }
  }

  onInput() {
    this.errors = this.validator.findFormErrors();
  }
}
