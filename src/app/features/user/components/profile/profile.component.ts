import { Component } from '@angular/core';
import { User } from '../../models/out/user';
import { firstValueFrom } from 'rxjs';
import { FetchService } from '@app/core/services/fetch-service/fetch.service';
import { AuthService } from '@app/core/services/auth-service/auth.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrl: './profile.component.scss'
})
export class ProfileComponent {
  constructor(private request : FetchService, private auth : AuthService) {}
  error: boolean = false;
  loading: boolean = true;
  user? : User;

  async ngOnInit() {
    try {
      this.user = await firstValueFrom(this.request.get<User>(`/api/v1/users/profile`));
    }
    catch(error) {
      this.error = true;
    }
    this.loading = false;
  }

  async deleteAccount() {
    this.loading = true;
    try {
      await firstValueFrom(this.request.delete(`/api/v1/users/delete_profile`));
      this.auth.logout();
    }
    catch(error) {
      this.error = true;
    }
    this.loading = false;
  }
}
