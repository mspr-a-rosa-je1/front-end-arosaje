import { Component } from '@angular/core';
import { Register } from '../../../../core/models/authentication';
import { FormGroup } from '@angular/forms';
import { AuthService } from '../../../../core/services/auth-service/auth.service';
import { ValidatorService } from '../../../../core/services/validator-service/validator.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.scss'
})
export class RegisterComponent {
  loading : boolean = false;
  check : boolean = false;
  errors : {[key: string]: string} = {};

  informations : Register = {
    email : "",
    password : "",
    confirmPassword : "",
    username : "",
    firstName : "",
    lastName : "",
    phone : ""
  };
  
  registerForm: FormGroup = new FormGroup({
    email: this.validator.createFormControl(this.informations.email, 'email'),
    password: this.validator.createFormControl(this.informations.password, 'password', {required : true}),
    confirmPassword: this.validator.createFormControl(this.informations.confirmPassword, 'confirmPassword'),
    username: this.validator.createFormControl(this.informations.username, 'username'),
    firstName: this.validator.createFormControl(this.informations.firstName, 'text'),
    lastName: this.validator.createFormControl(this.informations.lastName, 'text'),
    phone: this.validator.createFormControl(this.informations.phone, 'phone'),
    check : this.validator.createFormControl(this.check, 'check')
  })

  constructor(private auth : AuthService, private validator : ValidatorService) {
    this.validator.setForm(this.registerForm);
  }

  async onSubmit() {
    if(this.registerForm.valid) {
      this.loading = true;
      try {
        const informations = {...this.informations};
        delete informations.confirmPassword;
        await this.auth.register(informations);
      }
      catch(error) {
        this.errors['register'] = 'Impossible de créer le compte';
      }
      this.loading = false;
    }
  }

  onInput() {
    this.errors = this.validator.findFormErrors();
  }
}
