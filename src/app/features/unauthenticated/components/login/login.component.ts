import { Component } from '@angular/core';
import { Login } from '../../../../core/models/authentication';
import { AuthService } from '../../../../core/services/auth-service/auth.service';
import { FormGroup } from '@angular/forms';
import { ValidatorService } from '../../../../core/services/validator-service/validator.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})
export class LoginComponent {
  loading : boolean = false;

  login : Login = {
    email : "",
    password : ""
  };

  loginForm: FormGroup = new FormGroup({
    email: this.validator.createFormControl(this.login.email, 'email'),
    password: this.validator.createFormControl(this.login.password, 'password', {required : true})
  })

  errors : {[key: string]: string} = {};

  constructor(private auth : AuthService, private validator : ValidatorService) {
    this.validator.setForm(this.loginForm);
  }
  
  async onSubmit() {
    if(this.loginForm.valid) {
      this.loading = true;
      try {
        await this.auth.login(this.login);
        this.validator.removeForm();
      }
      catch(error) {
        this.errors['login'] = 'Mot de passe ou email incorrect';
      }
      this.loading = false;
    }
  }

  onInput() {
    this.errors = this.validator.findFormErrors();
  }
}
