import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MentionsComponent } from './components/mentions/mentions.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { ErrorComponent } from '../../shared/components/error/error.component';
import { InfoComponent } from './components/info/info.component';
import { HomeComponent } from './components/home/home.component';

const routes : Routes = [
  { 
    path: '',   
    redirectTo: 'home', 
    pathMatch: 'full' 
  },
  {
    path: "mentions-legale",
    component: MentionsComponent,
  },
  {
    path: "login",
    component: LoginComponent,
  },
  {
    path: "register",
    component: RegisterComponent,
  },
  {
    path: "not-found",
    component: ErrorComponent,
  },
  {
    path: "info",
    component: InfoComponent,
  },
  {
    path: "home",
    component: HomeComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class UnauthenticatedRoutingModule { }
