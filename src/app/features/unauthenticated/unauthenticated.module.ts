import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './components/home/home.component';
import { InfoComponent } from './components/info/info.component';
import { LoginComponent } from './components/login/login.component';
import { MentionsComponent } from './components/mentions/mentions.component';
import { RegisterComponent } from './components/register/register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SharedModule } from '../../shared/shared.module';
import { UnauthenticatedRoutingModule } from './unauthenticated-routing.module';



@NgModule({
  declarations: [
    HomeComponent,
    InfoComponent,
    LoginComponent,
    MentionsComponent,
    RegisterComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    SharedModule,
    UnauthenticatedRoutingModule,
  ]
})
export class UnauthenticatedModule { }
